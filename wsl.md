# Running DELPHI software on Windows
This section describes how to run DELPHI software on a desktop or laptop running Windows 10 or newer. This is possible using the WSL subsystem offered by Microsoft.
The following steps are needed:

* Enable WSL and choose a supported OS
* Install CVMFS. This step gives access to the software stack.
* Install EOS. This step gives access to the DELPHI data.

## Enable WSL
WSL is by default disabled. Please check the latest instructions form Microsoft on how to enable it.
At this point, you will be asked which Linux operating system you want to enable. In this example we will choose Ubuntu 22 or Ubuntu 24, which are both supported by DELPHI.
Documentation for Windows 10 is currently here:

[https://learn.microsoft.com/en-us/windows/wsl/install](https://learn.microsoft.com/en-us/windows/wsl/install)

## Install dependencies
The very first step is to update the repositories and install additional packages which are needed, such as tcsh, perl, the compilers etc:

```
sudo apt update
sudo apt -y install cmake gcc g++ gfortran libx11-dev libx11-dev libx11-6 libx11-6 libglu1-mesa libglu1-mesa-dev xutils-dev libmotif-dev libmotif-common r-base-dev tcsh xfonts-100dpi xfonts-75dpi libxfont2  xutils-dev libxbae-dev libxaw7-dev libssl-dev libglew-dev libdlm-dev
```

## Install CVMFS
CVMFS is a read only file system, on which you can find the DELPHI software binaries. Once installed and configure, it will look like an additional disk on your WSL system.

### Installing the software
First thing to know is that Ubuntu is like Debian, so we use the Debian distribution for it.

```
wget https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb
sudo dpkg -i cvmfs-release-latest_all.deb
rm -f cvmfs-release-latest_all.deb
```

Next, update the repositories and install the software:
```
sudo apt-get update
sudo apt-get install cvmfs
```

### Configuring the software
We need tell the auto mounter software to mount the file system, and tell cvmfs which areas we want:
To do that, run these commands:

```
sudo sed -i 's%#+dir:/etc/auto.master.d%+dir:/etc/auto.master.d%' /etc/auto.master
sudo echo "CVMFS_REPOSITORIES=delphi.cern.ch" > /etc/cvmfs/default.local
sudo echo "CVMFS_CLIENT_PROFILE=single" >> /etc/cvmfs/default.local
```

After a restart of the machine, the DELPHI software would be available, but let's first configure EOS as well.

## Install EOS
Data is stored on the EOS storage system at CERN, and can be access with the EOS client. As for CVMFS, it will look like yet another disk attached to your WSL system.

### Installing the software
You need to be root to create the required repositories. Let's first get the encryption key:

```
curl -sL http://storage-ci.web.cern.ch/storage-ci/storageci.key | sudo apt-key add -
```

The following step have to be done as root, so the easiest is to first get a root shell: 

```
sudo /bin/bash
```

Next, create the reposity (as root). In case of Ubuntu 22 please do:

```
echo "deb [arch=amd64] http://storage-ci.web.cern.ch/storage-ci/debian/eos/diopside/ jammy jammy/tag jammy/commit" > /etc/apt/sources.list.d/eos.list

```
If you are using Ubuntu 24, please replace "jammy" by "noble", like this:

```
echo "deb [arch=amd64] http://storage-ci.web.cern.ch/storage-ci/debian/eos/diopside/ noble noble/tag noble/commit" > /etc/apt/sources.list.d/eos.list
```

In case this gives a permission error, use an editor like pico or vi to create the file /etc/apt/sources.list.d/eos.list with the contents within the quotes above. Then,
still as root, update the repositories and install the eos software. 

```
apt update
apt install eos-client eos-fusex
```

### Configuring EOS:
Make sure that you are still root. There should be a folder named /etc/eos. Please check that it exists (via "ls -l /etc/eos"). In case it is not there, create it before proceeding:

```
mkdir /etc/eos
```

To configure EOS, we need to create three additional files:

```
echo "/eos    /etc/auto.eos" >> /etc/auto.master.d/eos.autofs
echo '{"appname":"lxplus","options":{"jsonstats":1},"name":"experiment","hostport":"eospublic.cern.ch","remotemountdir":"/eos/experiment/"}' > /etc/eos/fuse.experiment.conf
echo '{"appname":"lxplus","options":{"jsonstats":1},"name":"opendata","hostport":"eospublic.cern.ch","remotemountdir":"/eos/opendata/","auth":{"krb5":0}}' > /etc/eos/fuse.opendata.conf
```

Finally, still as root, create a new file named  /etc/auto.eos with the following contents:

```
experiment -fstype=eosx,fsname=experiment  :eosxd
opendata -fstype=eosx,fsname=opendata  :eosxd
```

Reboot the machine and start WSL again. Note that this can take quite a bit of time, depending on the speed of the network connection.

## Testing
Once the machine is back, start WSL again and check if all areas have been properly mounted:

```
ls /cvmfs/delphi.cern.ch
ls /eos/experiment/delphi
ls /eos/opendata/delphi
```

If that is the case, initialise the DELPHI environment manually first with

```
. /cvmfs/delphi.cern.ch/setup.sh

```

If that works fine, you can automate the initialisation of the setup by adding this line to the end of the ~/.profile file in your home directory, e.g.

```
echo ". /cvmfs/delphi.cern.ch/setup.sh" >> ~/.profile
```

A final test is to start up paw:

```
paw
```

That's all.

